# Ptyxis 47.2

 * Fix closing window title dialog with Return/Enter
 * Fix naming of Ubuntu palette
 * Allow setting shortcuts with Fn keys on Macbooks
 * Fix some solarized palette inconsistencies
 * Translation updates

# Ptyxis 47.1

 * More clipboard fixes for initial state tracking
 * Handle shell detection when directory slash is omitted
 * Exit cleanly when ptyxis-agent exits
 * Fix VteRegex warning from the search bar
 * Fix potential leak of VteTerminal instances
 * Update gruvbox light/dark scheme
 * Translation updates

# Ptyxis 47.0

 * Implement application open file vfunc to work with Nautilus
 * Fix inintial clipboard action state
 * Fix various styling issues
 * Rename Clone of Ubuntu to Ubuntu at request of Ubuntu
 * Try to adjust window size to keep grid size the same when
   adding or removing tabs

# Ptyxis 47.rc

This is a release for GNOME 47.rc

 * Ctrl+shift+w now set as default shortcut for closing a tab, matching
   GNOME Terminal.
 * Preferences window improvements.
 * Updated GNOME Terminal palettes.
 * UI changes to improve screen reader support.
 * Make CSS provider priority less than the GTK inspector.
 * Styling improvements to match Builder and Text Editor.
 * Fix support for enabling login shells.
 * Convenience API for distros which require downstream patching for
   current directory tracking (such as Debian).
 * Fix container tracking and notifications via termprops.
 * Show opacity toggle if the setting has been changed.
 * Handle new tab creation when directory is in a GVFS FUSE path.
 * Reduce agent poll priority when ^C is pressed.
 * Overview styling and live-preview improvements to updated immediately
   when preferences changes.
 * Fix missing shortcut bindings for reset and reset-and-clear.

# Ptyxis 47.beta

This is a beta release for GNOME 47

Chances since 47.alpha

 * Ptyxis no longer requires patches to VTE when using a recent VTE
   git tag for GNOME 47.
 * Legacy libc compat was dropped for 32-bit x86 as it is unnecessary.
 * Window pallete and styling improvements to match GNOME 47 styling.
 * A new gsetting to disable padding around the terminal, at the cost
   of runtime performance.
 * Translation updates.

# Ptyxis 47.alpha

This is our first pre-release for GNOME 47 as part of 47.alpha.

Chances since 46.0

 * Many updates for GNOME HIG across dialogs and widgetry.
 * Ptyxis can run the ptyxis-agent inside the sandbox if we fail to run
   on the host. That comes with drawbacks but at least keeps the application
   working in those scenarios.
 * The build system supports some whitebox renaming allowing it to be called
   "Terminal" in some situations. Distributions shipping Ptyxis as their
   terminal may be encouraged to use that. Branding is also reduced to less
   dramatic styling.
 * Many fixes for Podman, Toolbox, and Distrobox.
 * Fallback to `sh` if we cannot locate the shell specified in /etc/passwd.
 * Improvements to the tab parking lot so that processes are properly exited.
 * Notification improvements.
 * Port Ptyxis to use the new VTE "termprops" feature. This requires a newer
   VTE than is currently released but is anticipated shortly. This reduces
   the number of patches required by Ptyxis (and eventually Fedora) to a
   single small patch for vte.sh.in.
 * New palettes styles.
 * Many new command line options and improved support for combining them.
 * Translation updates.

Thanks to everyone who filed bugs and contributed patches!

-- Christian

